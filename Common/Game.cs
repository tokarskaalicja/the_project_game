using System;
using System.Collections.Generic;

namespace Common
{
    public abstract class Game
    {
        public Game()
        {
            Id = Guid.NewGuid();
            Players = new List<PlayerInfo>();
            Pieces = new List<PieceInfo>();
            Goals = new List<GoalInfo>();
        }

        Guid Id {get; set;}
        public List<PlayerInfo> Players {get; set;}
        public List<PieceInfo> Pieces {get; set;}
        public List<GoalInfo> Goals {get; set;}
        public int FirstScore {get;set;}=0;
        public int SecondScore{get;set;}=0;
    }

    public enum Direction
    {
        up,
        down,
        right,
        left
    }
}