using System;
namespace Common
{
    public class GoalInfo
    {

        public GoalInfo()
        {
            X = -1;
            Y = -1;
            IsDiscovered = false;
        }

        public int X {get; set;}
        public int Y {get; set;}
        public bool IsDiscovered {get; set;}
    }
}