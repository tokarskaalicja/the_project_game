namespace Common.Messages
{
    public class ActionInvalidMessage : MasterResponse
    {
        public ActionInvalidMessage(string recipientId, string reason)
        {
            Type="ACTION_INVALID";
            RecipientId=recipientId;
            SenderId="-1";
            Payload.Reason=reason;
        }
        public class PayloadClass
        {
            public string Reason{get;set;}
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}