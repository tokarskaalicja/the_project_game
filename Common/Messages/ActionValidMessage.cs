namespace Common.Messages
{
    public class ActionValidMessage : MasterResponse
    {
        public ActionValidMessage(string recipientId, int delay)
        {
            Type="ACTION_VALID";
            RecipientId=recipientId;
            SenderId="-1";
            Payload.Delay=delay;
        }
        public class PayloadClass
        {
            public int Delay{get;set;}
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}