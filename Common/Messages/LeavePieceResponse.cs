namespace Common.Messages
{
    public class LeavePieceResponse : MasterResponse
    {
        public LeavePieceResponse(string recipientID)
        {
            Type="PLACE_DOWN_PIECE_RESPONSE";
            RecipientId=recipientID;
            SenderId="-1";

        }
        public class PayloadClass
        {
            bool didCompleteGoal{get;set;}
        }
        public PayloadClass Payload {get;set;}=new PayloadClass();
    }
}