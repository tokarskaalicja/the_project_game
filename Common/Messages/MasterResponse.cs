﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Messages
{
    public class MasterResponse
    {
        public string Type { get; set; }
        public string SenderId { get; set; }
        public string RecipientId { get; set; }
    }
}
