namespace Common.Messages{
    public class MoveResponse : MasterResponse
    {
        public MoveResponse(string recipientId, int distance)
        {
            Type="MOVE_RESPONSE";
            SenderId="-1";
            RecipientId=recipientId;
            Payload.DistanceToPiece=distance;
        }
        public class PayloadClass
        {
            public int DistanceToPiece{get;set;}
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}