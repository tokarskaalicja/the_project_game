namespace Common.Messages
{
    public class PickUpResponse : MasterResponse
    {
        public PickUpResponse(string recipientId)
        {
            Type="PICK_UP_PIECE_RESPONSE";
            RecipientId=recipientId;
            SenderId="-1";
        }
    }
}