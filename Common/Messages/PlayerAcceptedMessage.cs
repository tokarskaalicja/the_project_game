﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Common.Messages
{
    public class PlayerAcceptedMessage : MasterResponse
    {
        public PlayerAcceptedMessage(string recipientId, string assignedPlayerId)
        {
            Type = "PLAYER_ACCEPTED";
            SenderId = "-1";
            RecipientId = recipientId;
            Payload.AssignedPlayerId = assignedPlayerId;
        }

        public class PayloadClass
        {
            public string AssignedPlayerId { get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}