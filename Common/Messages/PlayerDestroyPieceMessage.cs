﻿namespace Common.Messages
{
    public class PlayerDestroyPieceMessage : PlayerRequest
    {
        public PlayerDestroyPieceMessage(string senderId, string pieceId)
        {
            Type = "DESTROY_REQUEST";
            SenderId = senderId;
            Payload.PieceId = pieceId;
        }
        public class PayloadClass
        {
            public string PieceId { get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}