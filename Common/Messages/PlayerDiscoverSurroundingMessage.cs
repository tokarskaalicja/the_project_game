﻿namespace Common.Messages
{
    public class PlayerDiscoverSurroundingMessage : PlayerRequest
    {
        public PlayerDiscoverSurroundingMessage(string senderId)
        {
            Type = "DISCOVER_REQUEST";
            SenderId = senderId;
        }

    }
}
