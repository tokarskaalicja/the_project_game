﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Common.Messages
{
    public class PlayerHelloMessage : PlayerRequest
    {
        public PlayerHelloMessage(string senderId, string game, long teamId, bool isLeader, string temporaryId)
        {
            Type = "PLAYER_HELLO";
            SenderId = senderId;
            Payload.Game = game;
            Payload.TeamId = teamId;
            Payload.IsLeader = isLeader;
            Payload.TemporaryId = temporaryId;
        }

        public class PayloadClass
        {
            public string Game { get; set; }
            public long TeamId { get; set; }
            public bool IsLeader { get; set; }
            public string TemporaryId { get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}
