﻿namespace Common.Messages
{
    public class PlayerLeavePieceMessage : PlayerRequest
    {
        public PlayerLeavePieceMessage(string senderId, string pieceId)
        {
            Type = "LEAVE_PIECE_REQUEST";
            SenderId = senderId;
            Payload.PieceId = pieceId;
        }
        public class PayloadClass
        {
            public string PieceId { get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}