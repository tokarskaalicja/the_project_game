namespace Common.Messages
{
    public class PlayerMoveMessage : PlayerRequest
    {
        public PlayerMoveMessage(string senderId, Direction dir)
        {
            Type = "MOVE_REQUEST";
            SenderId = senderId;
            Payload.Direction = dir;
        }
        public class PayloadClass
        {
            public Direction Direction {get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}