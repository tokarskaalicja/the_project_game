﻿namespace Common.Messages
{
    public class PlayerPickPieceMessage : PlayerRequest
    {
        public PlayerPickPieceMessage(string senderId)
        {
            Type = "PICK_PIECE_REQUEST";
            SenderId = senderId;
        }
        
    }
}
