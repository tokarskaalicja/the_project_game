﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Common.Messages
{
    public class PlayerRejectedMessage : MasterResponse
    {
        public PlayerRejectedMessage(string recipientId, string reason)
        {
            Type = "PLAYER_REJECTED";
            SenderId = "-1";
            RecipientId = recipientId;
            Payload.Reason = reason;
        }

        public class PayloadClass
        {
            public string Reason { get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}
