﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Messages
{
    public class PlayerRequest
    {
        public string Type { get; set; }
        public string SenderId { get; set; }
    }

}
