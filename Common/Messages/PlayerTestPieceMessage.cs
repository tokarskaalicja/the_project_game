﻿namespace Common.Messages
{
    public class PlayerTestPieceMessage : PlayerRequest
    {
        public PlayerTestPieceMessage(string senderId, string pieceId)
        {
            Type = "TEST_REQUEST";
            SenderId = senderId;
            Payload.PieceId = pieceId;
        }
        public class PayloadClass
        {
            public string PieceId { get; set; }
        }
        public PayloadClass Payload { get; set; } = new PayloadClass();
    }
}