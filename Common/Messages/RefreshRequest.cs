namespace Common.Messages
{
    public class RefreshRequest : PlayerRequest
    {
        public RefreshRequest(string senderId)
        {
            Type = "REFRESH_STATE_REQUEST";
            SenderId = senderId;
        }
    }
}