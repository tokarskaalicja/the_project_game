using System;

namespace Common.Messages
{
    public class RefreshResponse : MasterResponse
    {
        public RefreshResponse(string recipientId, DateTime timestamp, int distanceToPiece, PlayerInfoShortened[] players, int team1Score, int team2Score)
        {
            Type="REFRESH_STATE_RESPONSE";
            SenderId="-1";
            RecipientId=recipientId;
            Payload.CurrentPositionDistanceToClosestPiece=distanceToPiece;
            Payload.Timestamp=timestamp;
            Payload.PlayerInfoShortened=players;
            Payload.Team1Score=team1Score;
            Payload.Team2Score=team2Score;
        }
        public class PayloadClass
        {
            public DateTime Timestamp{get;set;}  
            public int CurrentPositionDistanceToClosestPiece  {get;set;} 
           public PlayerInfoShortened[] PlayerInfoShortened{get;set;}
            public int Team1Score{get;set;}
            public int Team2Score{get;set;}
        }
        public PayloadClass Payload {get;set;}=new PayloadClass();
    }
}