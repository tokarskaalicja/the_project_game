using System;
namespace Common
{
    public class PieceInfo
    {
        public PieceInfo()
        {
            Id = Guid.NewGuid();
            IsSham = true;
            X = -1;
            Y = -1;
        }

        public Guid Id {get; set;}
        public bool IsSham {get; set;}
        public int X {get; set;}
        public int Y {get; set;}
        public bool IsHeld {get;set;}=false;
    }
}