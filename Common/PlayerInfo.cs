using System;
namespace Common
{
    public class PlayerInfo
    {
        public PlayerInfo()
        {
            Id = Guid.NewGuid();
            X = -1;
            Y = -1;
        }

        public Guid Id { get; private set; }
        public Team Team { get; set; }
        public Type Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }

    public enum Team
    {
        First,
        Second
    }
    public enum Type
    {
        Member,
        Leader
    }
}