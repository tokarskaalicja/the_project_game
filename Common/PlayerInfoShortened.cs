using System;
namespace Common
{
    public class PlayerInfoShortened
    {
        public Guid Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
