﻿using System;

namespace CommunicationServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = Server.Instance;
            server.AddPlayer(new Player.Player());
            server.AddPlayer(new Player.Player());
            server.AddPlayer(new Player.Player());
            server.AddPlayer(new Player.Player());
            server.AddPlayer(new Player.Player());
            server.AddPlayer(new Player.Player());

            server.SetMaster(new GameMaster.Master());
        }
    }
}
