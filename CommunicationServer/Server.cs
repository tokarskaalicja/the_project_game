﻿using GameMaster;
using Newtonsoft.Json.Linq;
using Player;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommunicationServer
{
    public class Server
    {
        private static Server server;
        private Server() {}

        public static Server Instance
        {
            get
            {
                if (server == null)
                    server = new Server();
                return server;
            }
        }

        private Master master;
        private List<Player.Player> players = new List<Player.Player>();

        public void AddPlayer(Player.Player player)
        {
            players.Add(player);
        }

        public void SetMaster(Master master)
        {
            this.master = master;
        }

        public void SendMessage(string message)
        {
            var msgObject = JObject.Parse(message);
            JToken value = null;
            bool valueSet = msgObject.TryGetValue("recipientId", out value);
            if (valueSet)
            {
                string recipientId = value.Value<string>();
                foreach (var player in players)
                {
                    if (player.Id == recipientId)
                    {
                        player.ReceiveMessage(message);
                        break;
                    }
                }
            }
            else
            {
                master.ReceiveMessage(message);
            }
        }
    }
}
