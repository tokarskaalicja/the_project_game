using System;
using System.Drawing;
using Common;
namespace GameMaster
{
    public class Game : Common.Game
    {
        private double shamProbability = 0.5;
        private double pieceFrequency = 0.5;
        private int pieceInitialNumber = 3;
        private int MaxPlayerNumberInTeam = 3;
        public int MaxPlayerNumber => MaxPlayerNumberInTeam * 2;
        public int BoardWidth {get; set;} = 10;
        public int TaskAreaLength {get; set;} = 10;
        public int GoalAreaLength { get; set;} = 2;
        private int numberOfGoals = 2;
        private int LeaderCount { get; set; } = 0;
        private int FirstTeamPlayerCount { get; set; } = 0;
        private int SecondTeamPlayerCount { get; set; } = 0;
        private Random random = new Random();

        public PlayerInfo AddPlayer(bool isLeader)
        {
            if (!AddPlayerPossible(isLeader))
                return null;
            var playerInfo = new PlayerInfo();
            AssignTeam(playerInfo);
            AssignType(playerInfo, isLeader);
            AddPlayerPosition(playerInfo);
            Players.Add(playerInfo);
            return playerInfo;
        }

        private void AssignTeam(PlayerInfo playerInfo)
        {
             if (SecondTeamPlayerCount < FirstTeamPlayerCount)
            {
                playerInfo.Team = Team.Second;
                SecondTeamPlayerCount++;
            }
            else
            {
                playerInfo.Team = Team.First;
                FirstTeamPlayerCount++;
            }
        }

        private void AssignType(PlayerInfo playerInfo, bool isLeader)
        {
            if (isLeader)
            {
                playerInfo.Type = Common.Type.Leader;
                LeaderCount++;
            }
            else
                playerInfo.Type = Common.Type.Member;
        }

        private bool AddPlayerPossible(bool isLeader)
        {
             if (isLeader)
            {
                if (LeaderCount == 2)
                {
                    return false;
                }
            }
            if (Players.Count == MaxPlayerNumber)
            {
                return false;
            }
            if (!isLeader && MaxPlayerNumber - Players.Count == 2 - LeaderCount)
                return false;
            return true;
        }

        private void AddPlayerPosition(PlayerInfo playerInfo)
        {
            Random random = new Random();
            if (playerInfo.Team == Team.First)
                playerInfo.Y = GoalAreaLength - 1;
            else
                playerInfo.Y = GoalAreaLength + TaskAreaLength;
            playerInfo.X = random.Next(0, BoardWidth - 1);
            if (Players.Count == 0)
                return;
            int iterationCounter = 0;
            while (Players.Find(p => p.X == playerInfo.X && p.Y == playerInfo.Y) != null)
            {
                playerInfo.X = random.Next(0, BoardWidth - 1);
                iterationCounter++;
                if (iterationCounter > 10 * BoardWidth)
                {
                    if (playerInfo.Team == Team.First)
                        playerInfo.Y--;
                    else
                        playerInfo.Y++;
                    iterationCounter = 0;
                }
                if (playerInfo.Y > GoalAreaLength * 2 + TaskAreaLength || playerInfo.Y < 0)
                    return;
            }

        }
        public PieceInfo AddPiece()
        {
            PieceInfo pieceInfo = new PieceInfo();

            Random random = new Random();
            double r = random.NextDouble();

            int iterationCounter = 0;

            if (r <= shamProbability)
            {
                pieceInfo.IsSham = true;
            }

            pieceInfo.X = random.Next(0, BoardWidth - 1);
            pieceInfo.Y = random.Next(GoalAreaLength, GoalAreaLength + TaskAreaLength - 1);

            while (!isFieldEmpty(pieceInfo.X, pieceInfo.Y))
            {
                pieceInfo.X = random.Next(0, BoardWidth - 1);
                pieceInfo.Y = random.Next(GoalAreaLength, GoalAreaLength + TaskAreaLength - 1);

                iterationCounter++;

                if (iterationCounter > BoardWidth * TaskAreaLength * 2)
                {
                    //if(Pieces.Count + Players.Count == BoardWidth * TaskAreaLength)
                        return null;
                }

            }
            Pieces.Add(pieceInfo);
            return pieceInfo;
        }

        private bool isFieldEmpty(int X, int Y)
        {
            return Pieces.Find(x => x.X == X && x.Y == Y) == null && Players.Find(x => x.X == X && x.Y == Y) == null;
        }

        public GoalInfo AddFirstGoal()
        {
            //first players on top
            return AddGoal(0, GoalAreaLength-1);
        }

        public GoalInfo AddSecondGoal()
        {
            //second players on bottom
            return AddGoal(GoalAreaLength + TaskAreaLength, 2 * GoalAreaLength + TaskAreaLength - 1);
        }

        private GoalInfo AddGoal(int minY, int maxY)
        {
            GoalInfo goalInfo = new GoalInfo();
            Random random = new Random();
            int iterationCounter = 0;

            goalInfo.X = random.Next(0, BoardWidth - 1);
            goalInfo.Y = random.Next(minY, maxY);

            while (Goals.Find(x => x.X == goalInfo.X && x.Y == goalInfo.Y) != null)
            {
                goalInfo.X = random.Next(0, BoardWidth - 1);
                goalInfo.Y = random.Next(minY, maxY);

                iterationCounter++;

                if (iterationCounter > BoardWidth * GoalAreaLength * 2)
                    return null;
            }
            return goalInfo;
        }

        //chyba testowa funkcja
        public void AssignLeaders()
        {
            AddPlayer(true);
            AddPlayer(true);
        }
        public bool GameConditionsFulfilled()
        {
            if (Players.Count != MaxPlayerNumber || Pieces.Count != pieceInitialNumber || Goals.Count != numberOfGoals * 2)
                return false;
            if (FirstTeamPlayerCount != MaxPlayerNumberInTeam || SecondTeamPlayerCount != MaxPlayerNumberInTeam)
                return false;
            if (Players.Find(x => (x.Type == Common.Type.Leader && x.Team == Team.First)) == null || Players.Find(x => (x.Type == Common.Type.Leader && x.Team == Team.Second)) == null)
                return false;
            return true;
        }

        public Game Create()
        {
            //doesnt pass tests

            //load arguments from configuration file?
            // Game game = new Game();//po co jak mozna zwrocic this?

            //adding goals
            for (int i = 0; i < numberOfGoals; i++)
            {
                GoalInfo goalInfo = new GoalInfo();
                goalInfo = AddFirstGoal();
                if (goalInfo != null) Goals.Add(goalInfo);

                goalInfo = AddSecondGoal();
                if (goalInfo != null) Goals.Add(goalInfo);
            }

            //adding pieces
            for (int i = 0; i < pieceInitialNumber; i++)
            {
                AddPiece();
            }

            //adding players
            //adding liders
            AssignLeaders();

            PlayerInfo player;
            //adding  regular players
            for (int i = 0; i < MaxPlayerNumber - 2; i++)
            {
                AddPlayer(false);
            }

            return this;
        }

        public int MovePlayer(string playerId, Direction dir)
        {
            PlayerInfo player = Players.Find(x => x.Id.ToString() == playerId);
            if (player == null) return -1;
            Point newPos=new Point(player.X, player.Y);
            if(player.Team==Team.Second)
                if(dir==Direction.right)
                    dir=Direction.left;
                else if(dir==Direction.up)
                    dir=Direction.down;
                else if(dir==Direction.down)
                    dir=Direction.up;
                else 
                    dir=Direction.right;
            switch(dir){
                case Direction.down:
                    newPos.Y--;
                    break;
                case Direction.up:
                    newPos.Y++;
                    break;
                case Direction.right:
                    newPos.X++;
                    break;
                case Direction.left:
                    newPos.X--;
                    break;
            }
            if(isMovementPossible(newPos, player.Team))
            { 
                player.X=newPos.X;
                player.Y=newPos.Y;
                return CalculateShortestDistance(player.X, player.Y);
            }
           return -1;
        }
        private bool isMovementPossible(Point point, Team site)
        {
            if(point.X<0 || point.X>BoardWidth)
                return false;
            if(site==Team.First)
            {      
                if(point.Y < 0 || point.Y >= GoalAreaLength + TaskAreaLength)
                    return false;
            }
            else
                if(point.Y<GoalAreaLength || point.Y>= 2*GoalAreaLength+TaskAreaLength)
                    return false;
            if(Players.Find(x=>x.X==point.X && x.Y==point.Y)!=null)
                return false;
            //MOVEMENT POSSIBLE!!!!!!!!!
            return true;
        }
        public int CalculateShortestDistance(int X, int Y)
        {
            int min = Int16.MaxValue;
            foreach(var piece in Pieces)
            {
                if(!piece.IsHeld)
                {
                    int dist = Math.Abs(X-piece.X) + Math.Abs(Y - piece.Y);
                    if ( dist < min)
                        min = dist;
                }
            }
            return min;
        }

        public int GetPlayerShortestDistance(string playerId)
        {
            PlayerInfo player = Players.Find(x=> x.Id.ToString()==playerId);
            return CalculateShortestDistance(player.X, player.Y);
        }

        public PlayerInfoShortened[] GetShortenedInfo()
        {
            PlayerInfoShortened[] info = new PlayerInfoShortened[Players.Count];
            for(int i = 0; i < Players.Count; i++)
            {
                info[i] = new PlayerInfoShortened(){Id = Players[i].Id, X = Players[i].X, Y = Players[i].Y };
            }
            return info;
        }
        public bool IsPlayerOnPiece(string playerID)
        {
            PlayerInfo pi=Players.Find(x=>x.Id.ToString()==playerID);
            if(pi==null)return false;
            PieceInfo piece=Pieces.Find(x=>x.X==pi.X && x.Y==pi.Y);
            if(piece==null)
                return false;
            piece.IsHeld=true;
            return true;
        }
        public bool? TryToPlacePieceOnGoal(string playerID)
        {
            PlayerInfo pi=Players.Find(x=>x.Id.ToString()==playerID);
            if(pi==null)return null;
            PieceInfo pieceInfo=Pieces.Find(x=>x.X==pi.X && x.Y==pi.Y);
            if(pieceInfo==null)return null;
            if(pieceInfo.IsSham) return null; //undefined
            GoalInfo gi = Goals.Find(x=>x.X==pi.X && x.Y==pi.Y);
            if(gi==null || gi.IsDiscovered)
                return false;

            //is undiscovered goal
            if(pi.Team==Team.First)
                FirstScore++;
            else
                SecondScore++;
            if(IsGameFinished())
            {
                //sync Server.FinishGame
            }

            gi.IsDiscovered=true;
            return true; 
        }
        private bool IsGameFinished()
        {
            return FirstScore >= numberOfGoals || SecondScore >= numberOfGoals;
        }
    }
}