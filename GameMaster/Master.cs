using System;
using Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Common.Messages;
using Newtonsoft.Json.Serialization;
//using CommunicationServer;

namespace GameMaster
{
    public class Master
    {
        public Game Game {get; set;}
        //public Server server {get; set;}

        public Master()
        {
            Game = new Game();
            
        }

        public string ReceiveMessage(string message)
        {
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var serializer = new JsonSerializer();
            serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var obj = JObject.Parse(message);
            string messageType = obj["type"].Value<string>();
                
            MasterResponse response = null;
            if (messageType == "PLAYER_HELLO")
            {
                response = HandlePlayerHelloMessage(obj.ToObject<PlayerHelloMessage>(serializer));
            }
            else if (messageType == "MOVE_REQUEST")
            {
                response = HandlePlayerMoveMessage(obj.ToObject<PlayerMoveMessage>(serializer));
            }
            else if (messageType == "TEST_REQUEST")
            {
                response = HandlePlayerTestPieceMessage(obj.ToObject<PlayerTestPieceMessage>(serializer));
            }
            else if(messageType=="REFRESH_STATE_REQUEST")
            {
                response=HandlePlayerRefreshMessage(obj.ToObject<RefreshRequest>(serializer));
            }
            else if(messageType=="PICK_UP_PIECE_REQUEST")
            {
                response=HandlePlayerPickUpMessage(obj.ToObject<PlayerPickPieceMessage>(serializer));
            }
            if (response != null)
                return JsonConvert.SerializeObject(response, settings);
            return null;
        }
        
        private MasterResponse HandlePlayerHelloMessage(PlayerHelloMessage msg)
        {
            PlayerInfo playerInfo = Game.AddPlayer(msg.Payload.IsLeader);
            if (playerInfo != null)
            {
                var playerAcceptedMessage = new PlayerAcceptedMessage(msg.Payload.TemporaryId, playerInfo.Id.ToString());
                return playerAcceptedMessage;
            }
            else
            {
                var playerRejectedMessage = new PlayerRejectedMessage(msg.Payload.TemporaryId, "Too many players");
                return playerRejectedMessage;
            }
        }
        private MasterResponse HandlePlayerMoveMessage(PlayerMoveMessage msg)
        {
            int result = Game.MovePlayer(msg.SenderId, msg.Payload.Direction);
            if (result != -1)
            {
                var actionValidMessage = new ActionValidMessage(msg.SenderId, 500);

                MoveResponse moveResponse=new MoveResponse(msg.SenderId, result);

                //Server.SendMessage(JsonConvert.SerializeObject(actionvalidMessage))
                //Delay(delay)
                return moveResponse;
                //return actionValidMessage;
            }
            else
            {
                var actionInvalidMessage = new ActionInvalidMessage(msg.SenderId, "impossible movement");
                return actionInvalidMessage;
            }
        }
        private MasterResponse HandlePlayerRefreshMessage(RefreshRequest request)
        {
            int distance = Game.GetPlayerShortestDistance(request.SenderId);
            int team1Score = Game.FirstScore;
            int team2Score = Game.SecondScore;
            DateTime timestamp = GetTimestamp();
            PlayerInfoShortened[] info = Game.GetShortenedInfo();
            RefreshResponse response=new RefreshResponse(request.SenderId, timestamp, distance, info, team1Score, team2Score);
            return response;
        }
        private MasterResponse HandlePlayerTestPieceMessage(PlayerTestPieceMessage msg)
        {
            bool isGood;
            for(int i=0; i<Game.Pieces.Count; i++)
            {
                if(msg.Payload.PieceId.Equals(Game.Pieces[i].Id.ToString()))
                {
                    if(Game.Pieces[i].IsSham)
                    {
                        isGood = false;
                    }
                    else
                    {
                        isGood = true;
                    }
                    return new ActionValidMessage(msg.SenderId, 500);
                }

            }
            return new ActionInvalidMessage(msg.SenderId, "piece not found");
        }
        private MasterResponse HandlePlayerPickUpMessage(PlayerPickPieceMessage msg)
        {
            if(Game.IsPlayerOnPiece(msg.SenderId))
            {
                //server.call(actionvalid)
                //delay
                PickUpResponse response = new PickUpResponse(msg.SenderId);
                return response;
            }
            else
            {
                return new ActionInvalidMessage(msg.SenderId, "there's no piece");
            }
            
        }
        // private MasterResponse HandlePlayerPlaceDownPieceMessage(PlayerLeavePieceMessage message)
        // {

        //     LeavePieceResponse leavePieceResponse;
        //     if(Game.TryToPlacePieceOnGoal(message.SenderId))
        //     {
        //         //timescore up
        //         //true
                
        //     }
        //     else
        //     {

        //     }
        //     return leavePieceResponse;
        // }
        private DateTime GetTimestamp()
        {
            return DateTime.Now;
        }
    }
}