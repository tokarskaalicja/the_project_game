﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Player
{
   public class Game:Common.Game
    {
        public int BoardWidth { get; set; } = 10;
        public int TaskAreaLength { get; set; } = 10;
        public int GoalAreaLength { get; set; } = 2;
        public bool active = false;
        public Dictionary<Tuple<int, int>, int> pieceDistances=new Dictionary<Tuple<int, int>, int>();
    }
}
