﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Common.Messages;

namespace Player
{
    public class Player
    {
        public string Id { get; set; }
        private Game game { get; }
     
        private Common.PlayerInfo myInfo { get; set; }

        public bool carryPiece { get; set; }
        public string pieceId { get; set; }

        public Player()
        {
            game = new Game();
            Id = Guid.NewGuid().ToString();
            carryPiece = false;
        }
        public string ReceiveMessage(string message)
        {
            var serializer = new JsonSerializer();
            serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var obj = JObject.Parse(message);
            string messageType = obj["type"].Value<string>();
            
            if (messageType == "PLAYER_ACCEPTED")
            {
                HandlePlayerAcceptedMessage(obj.ToObject<PlayerAcceptedMessage>(serializer));
            }
            return null;
        }

        private void HandlePlayerAcceptedMessage(PlayerAcceptedMessage playerAcceptedMessage)
        {
            
        }

        //TO IMPLEMENT
        private void SendMessage(Common.Messages.PlayerRequest pr)
        {

        }

        private void StartGame()
        {
            RefreshGameState();
            DecideNextStep();
        }
        private void DecideNextStep()
        {
            if (!game.active)
                return;
            if(carryPiece)
            {
                if (myInfo.Y < game.GoalAreaLength || myInfo.Y >= game.GoalAreaLength + game.TaskAreaLength)
                    LookForGoal();
                if (myInfo.Team == Common.Team.First)
                    Move(Common.Direction.down);
                else
                    Move(Common.Direction.up);
            }
            else
            {
                int pieceDistance = -1; 
                if (myInfo.Y < game.GoalAreaLength || myInfo.Y >= game.GoalAreaLength + game.TaskAreaLength)
                {
                    if (myInfo.Team == Common.Team.First)
                        pieceDistance = Move(Common.Direction.up);
                    else
                        pieceDistance = Move(Common.Direction.down);
                    if (pieceDistance == 0)
                    {
                        TakePiece();
                        if (!TestPiece())
                        {
                            DestroyPiece();
                        }
                                           
                    }
                }
                else
                {
                    LookForPiece();
                }
            }
        }
        private void LookForGoal()
        {
            Tuple<int, int> emptyFieldPossition = getEmptyGoalAreaField();
            Common.Direction firstDirection = emptyFieldPossition.Item1 - myInfo.X > 0 ? Common.Direction.up : Common.Direction.down;
            Common.Direction secondDirection = emptyFieldPossition.Item2 - myInfo.Y > 0 ? Common.Direction.right : Common.Direction.left;
            while (emptyFieldPossition.Item1 - myInfo.X != 0)
            {
                if (Move(firstDirection) == -1)
                    break;
            }
            while (emptyFieldPossition.Item2 - myInfo.Y != 0)
            {
                if (Move(secondDirection) == -1)
                    break;
            }
            NotifyPieceLeft(emptyFieldPossition, LeavePiece());  // ?? 
            
        }
        private Tuple<int,int> getEmptyGoalAreaField()
        {
            return null;
        }
        private void LookForPiece()
        {
            int pieceDistance = -1;
            DiscoverSurrounding();
            Tuple<int, int> targetPossition = game.pieceDistances.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            if (targetPossition.Item1 - myInfo.X == 0)
                if (targetPossition.Item2 - myInfo.Y > 0)
                   pieceDistance=Move(Common.Direction.up);
                else
                   pieceDistance=Move(Common.Direction.down);
            else if(targetPossition.Item2 - myInfo.Y == 0)
                if (targetPossition.Item1 - myInfo.X > 0)
                    pieceDistance = Move(Common.Direction.right);
                else
                    pieceDistance = Move(Common.Direction.left);
            else
            {
                if(targetPossition.Item1-myInfo.X>0)
                    Move(Common.Direction.right);              
                else
                    Move(Common.Direction.left);

                if (targetPossition.Item2 - myInfo.Y > 0)
                   pieceDistance=Move(Common.Direction.up);
                else
                    pieceDistance=Move(Common.Direction.down);
            }

            if (pieceDistance == 0)
            {
                TakePiece();
                if (!TestPiece())
                    DestroyPiece();
                else
                    carryPiece = true;
            }
        }
        private void RefreshGameState()//also info about player possition at start
        {

        }
        public int Move(Common.Direction direction)//return received distance to the nearest piece
        {
            Common.Messages.PlayerMoveMessage move = new Common.Messages.PlayerMoveMessage(Id, direction);
            //SendMessage();
            int distance = 2; 
            //distance = ReceiveMessage();
            return distance;
        }

        public void TakePiece()
        {
            Common.Messages.PlayerPickPieceMessage pick = new Common.Messages.PlayerPickPieceMessage(Id);
            //SendMessage();
            string id = "";
            //id = ReceiveMessage();
            //if message positive
            carryPiece = true;
            pieceId = id;
        }
        
        public bool TestPiece()
        {
            Common.Messages.PlayerTestPieceMessage test = new Common.Messages.PlayerTestPieceMessage(Id, pieceId);
            //SendMessage();
            //ReceiveMessage();
            //if message positive
            //return true;
            return false;
        }

        public bool LeavePiece()//true-piece left in a good goal
        {
            Common.Messages.PlayerLeavePieceMessage leave = new Common.Messages.PlayerLeavePieceMessage(Id, pieceId);
            //SendMessage();
            bool goalDiscovered = false;
            //goalDiscovered = ReceiveMessage();
            return goalDiscovered;
        } 

        public void DestroyPiece()
        {
            Common.Messages.PlayerDestroyPieceMessage destroy = new Common.Messages.PlayerDestroyPieceMessage(Id, pieceId);
            //SendMessage();
            //ReceiveMessage();
            //if message positive:
            pieceId = "";
            carryPiece = false;
        }

        private void DiscoverSurrounding()//also refill pieceDistance dictionary
        {
            game.pieceDistances.Clear();
            Common.PlayerInfo[] players = new Common.PlayerInfo[9];
            Common.Messages.PlayerDiscoverSurroundingMessage discover = new Common.Messages.PlayerDiscoverSurroundingMessage(Id);
            //SendMessage();
            //ReceiveMessage();
            //if message positive
            //players=ReceiveMessage();
            //game.pieceDistances=ReceiveMessage();
            bool playerUpdated = false;
            for(int i=0; i<players.Length; i++)
            {
                for(int j=0; j<game.Players.Count; j++)
                {
                    if(players[i].Id.Equals(game.Players[j]))
                    {
                        game.Players[j] = players[i];
                        playerUpdated = true;
                    }
                    if (!playerUpdated)
                    {
                        game.Players.Add(players[i]);
                        playerUpdated = false;
                    }
                }
            }
            
        }
       private void NotifyPieceLeft(Tuple<int,int> possition,bool isGoal) // ??
        {

        }

    }
}
