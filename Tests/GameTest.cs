using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
namespace Tests
{
    [TestClass]
    public class GameTest
    {
        [TestMethod]
        public void MasterGameConstructor()
        {
            GameMaster.Game game = new GameMaster.Game();
        }

        [TestMethod]
        public void PlayerGameConstructor()
        {
            // Player.Game game = new Player.Game();
        }

        [TestMethod]
        public void AddTooManyPlayers()
        {
            GameMaster.Game game = new GameMaster.Game();

            game.AddPlayer(true);
            game.AddPlayer(true);

            for (int i = 0; i < game.MaxPlayerNumber - 2; i++)
            {
                game.AddPlayer(false);
            }
            Assert.IsNull(game.AddPlayer(false), "More than max players added to game");
        }
        public void TooManyRegularPlayers()
        {
            GameMaster.Game game = new GameMaster.Game();

            for (int i = 0; i < game.MaxPlayerNumber - 2; i++)
            {
                game.AddPlayer(false);
            }
            Assert.IsNull(game.AddPlayer(false), "More than max regular players added to game");
        }
        [TestMethod]
        public void AddOnePlayer()
        {
            GameMaster.Game game = new GameMaster.Game();
            var playerInfo = game.AddPlayer(false);

            Assert.IsNotNull(playerInfo, "Player addition unsuccessful");
            Assert.AreEqual(playerInfo.Type, Common.Type.Member, "Wrong type of player");

            Assert.AreNotEqual(-1, playerInfo.X, "Wrong starting player position");
            Assert.AreNotEqual(-1, playerInfo.Y, "Wrong starting player position");
        }
        [TestMethod]
        public void addOneLeader()
        {
            GameMaster.Game game = new GameMaster.Game();
            var playerInfo = game.AddPlayer(true);
            Assert.IsNotNull(playerInfo, "Leader adition unsuccesful");
            Assert.AreEqual(playerInfo.Type, Common.Type.Leader, "Wrong type of player");
        }
        [TestMethod]
        public void addTwoLeaders()
        {
            GameMaster.Game game = new GameMaster.Game();

            var playerInfo1 = game.AddPlayer(true);
            var playerInfo2 = game.AddPlayer(true);

            Assert.IsNotNull(playerInfo1, "Leader adition unsuccesful");
            Assert.IsNotNull(playerInfo2, "Leader adition unsuccesful");

            Assert.AreEqual(playerInfo1.Type, Common.Type.Leader, "Wrong type of player");
            Assert.AreEqual(playerInfo2.Type, Common.Type.Leader, "Wrong type of player");

            Assert.AreNotEqual(playerInfo1.Team, playerInfo2.Team, "Two leaders are in the same team");
        }
        [TestMethod]
        public void addTooManyLeaders()
        {
            GameMaster.Game game = new GameMaster.Game();

            var playerInfo1 = game.AddPlayer(true);
            var playerInfo2 = game.AddPlayer(true);
            Assert.IsNull(game.AddPlayer(true), "Too many leaders");
        }
        [TestMethod]
        public void RangePossition()
        {
            GameMaster.Game game = new GameMaster.Game();
            game.AssignLeaders();
            for (int i = 0; i < game.MaxPlayerNumber - 2; i++)
                game.AddPlayer(false);
            Assert.IsNull(game.Players.Find(x => x.X < 0 || x.X >= game.BoardWidth), "bad x possition");
            Assert.IsNull(game.Players.Find(x => x.Y < 0 || x.Y >= game.GoalAreaLength*2 + game.TaskAreaLength
                                                    || (x.Y >= game.GoalAreaLength && x.Y < game.GoalAreaLength + game.TaskAreaLength)),
                                                    "Bad Y possition");

        }
        [TestMethod]
        public void TeamPossition()
        {
            GameMaster.Game game = new GameMaster.Game();
            game.AddPlayer(true);
            game.AddPlayer(true);

            for (int i = 0; i < game.MaxPlayerNumber - 2; i++)
            {
                game.AddPlayer(false);
                if (game.Players[i + 2].Team == Team.First)
                    Assert.AreEqual(game.Players[i + 2].Y, game.Players[i].Y, "Wrong Y possition of the first team");
                else
                    Assert.AreEqual(game.Players[i + 2].Y, game.Players[i].Y, "Wrong Y possition of the second team");
            }

            Assert.AreEqual(0, game.Players.FindAll(x => x.Team == Team.First).GroupBy(c => c.X).Where(g => g.Count() > 1).SelectMany(g => g).Count(), "There are few first team players in the same possiotion");
            Assert.AreEqual(0, game.Players.FindAll(x => x.Team == Team.Second).GroupBy(c => c.X).Where(g => g.Count() > 1).SelectMany(g => g).Count(), "There are few second team players in the same possiotion");
        }

        [TestMethod]
        public void AddOnePiece()
        {
            GameMaster.Game game = new GameMaster.Game();
            var pieceInfo = game.AddPiece();

            Assert.IsNotNull(pieceInfo, "Piece addition unsuccessful - many fields occupied");

            Assert.AreNotEqual(-1, pieceInfo.X, "Wrong starting piece position");
            Assert.AreNotEqual(-1, pieceInfo.Y, "Wrong starting piece position");
        }

        [TestMethod]
        public void AddOneFirstGoal()
        {
            GameMaster.Game game = new GameMaster.Game();
            var goalInfo = game.AddFirstGoal();

            Assert.IsNotNull(goalInfo, "Goal addition unsuccessful - many fields occupied");

            Assert.AreNotEqual(-1, goalInfo.X, "Wrong goal position");
            Assert.AreNotEqual(-1, goalInfo.Y, "Wrong goal position");
        }

        [TestMethod]
        public void AddOneSecondGoal()
        {
            GameMaster.Game game = new GameMaster.Game();
            var goalInfo = game.AddSecondGoal();

            Assert.IsNotNull(goalInfo, "Goal addition unsuccessful - many fields occupied");

            Assert.AreNotEqual(-1, goalInfo.X, "Wrong goal position");
            Assert.AreNotEqual(-1, goalInfo.Y, "Wrong goal position");
        }

        [TestMethod]
        public void CreateGame()
        {
            GameMaster.Game game = new GameMaster.Game();

            game = game.Create();

            Assert.AreNotEqual(0, game.Pieces.Count, "No pieces added");
            Assert.AreNotEqual(0, game.Goals.Count, "No goals added");
            Assert.AreEqual(3, game.Pieces.Count, "Wrong number of pieces added");
            Assert.AreEqual(4, game.Goals.Count, "Wrong number of goals added");


        }

        [TestMethod]
        public void CheckGameConditions()
        {
            GameMaster.Game game = new GameMaster.Game();
            game.AddPlayer(false);

            Assert.IsFalse(game.GameConditionsFulfilled(), "Game wants to start with one player");

            for (int i = 0; i < game.MaxPlayerNumber - 3; i++)
            {
                game.AddPlayer(false);
            }

            Assert.IsFalse(game.GameConditionsFulfilled(), "Game wants to start without leaders");

            game.AssignLeaders();

            game = game.Create();

            Assert.IsTrue(game.GameConditionsFulfilled(), "Game doesn't start");
        }
        [TestMethod]
        public void MovePlayerAbroadTest()
        {
            GameMaster.Game game=new GameMaster.Game();
            PlayerInfo pi=game.AddPlayer(false);
            pi.Y=1;
            pi.X=0;
            Assert.AreNotEqual(-1,game.MovePlayer(pi.Id.ToString(),Direction.down));
            Assert.AreEqual(-1,game.MovePlayer(pi.Id.ToString(),Direction.down));
        }
        [TestMethod]
        public void CalculateDistanceToPieceTest()
        {
            GameMaster.Game game=new GameMaster.Game();
            PieceInfo pi1=game.AddPiece();
            PieceInfo pi2=game.AddPiece();
            pi1.X=pi1.Y=0;
            pi2.X=pi2.Y=5;
            Assert.AreEqual(10, game.CalculateShortestDistance(15,5));
        }
        [TestMethod]
        public void MovePlayerOnPlayerTest()
        {
            GameMaster.Game game=new GameMaster.Game();
            PlayerInfo pi=game.AddPlayer(false);
            PlayerInfo pi2=game.AddPlayer(false);
            pi.Y=1;
            pi.X=0;
            pi2.Y=1;
            pi2.X=2;
            Assert.AreNotEqual(-1,game.MovePlayer(pi.Id.ToString(),Direction.right));
            Assert.AreEqual(-1,game.MovePlayer(pi.Id.ToString(),Direction.right));
        }
    }
}
