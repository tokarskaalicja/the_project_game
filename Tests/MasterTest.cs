using System;
using Common;
using Common.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Tests
{
    [TestClass]
    public class MasterTest
    {
        [TestMethod]
        public void MasterGameCreate()
        {
            GameMaster.Master master = new GameMaster.Master();
        }

        [TestMethod]
        public void MasterHandlePlayerHelloMessage()
        {
            string message = @"{
                ""type"": ""PLAYER_HELLO"",
                ""senderId"": -2,
                ""payload"": {
                    ""game"": ""Default"",
                    ""teamId"": 1,
                    ""isLeader"": true,
                    ""temporaryId"": 45645641568
                }
            }";

            GameMaster.Master master = new GameMaster.Master();
            var response = master.ReceiveMessage(message);

            JObject responseObject = JObject.Parse(response);
            Assert.AreEqual("PLAYER_ACCEPTED", responseObject["type"]);
            Assert.AreEqual(1, master.Game.Players.Count);
        }

        [TestMethod]
        public void MasterHandleMoveMessage()
        {
            var serializer = new JsonSerializer();
            serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();            
            GameMaster.Master master = new GameMaster.Master();
            PlayerInfo pi = master.Game.AddPlayer(false);
            pi.X = 0;
            pi.Y = 0;
            string message = @"{
                ""type"": ""MOVE_REQUEST"",
                ""senderId"":""" + pi.Id.ToString() + @""",
                ""payload"": {
                    ""direction"": ""up""
                }
            }";
            PieceInfo info = master.Game.AddPiece();
            Console.WriteLine(info.X + ", " + info.Y);
            int distance = info.X + Math.Abs(info.Y-1);
            var response = (master.ReceiveMessage(message));

            JObject responseObject = JObject.Parse(response);
            MoveResponse msg = responseObject.ToObject<MoveResponse>(serializer);
            Assert.AreEqual("MOVE_RESPONSE", responseObject["type"]);
            Assert.AreEqual(distance, msg.Payload.DistanceToPiece);
        }

        [TestMethod]
        public void MasterHandleHandlePlayerTestPieceMessage()
        {

            GameMaster.Master master = new GameMaster.Master();
            PlayerInfo pi = master.Game.AddPlayer(false);
            pi.X = 0;
            pi.Y = 0;
            string message = @"{
                ""type"": ""TEST_REQUEST"",
                ""senderId"":""" + pi.Id.ToString() + @""",
                ""payload"": {
                    ""pieceId"": ""1""
                }
            }";
            Console.WriteLine(message);
            var response = master.ReceiveMessage(message);

            JObject responseObject = JObject.Parse(response);
            Assert.AreEqual("ACTION_INVALID", responseObject["type"]);
        }
        [TestMethod]
        public void RefreshResponseTest()
        {
             var serializer = new JsonSerializer();
            serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();            
           GameMaster.Master master=new GameMaster.Master();
            PlayerInfo pi= master.Game.AddPlayer(false);
            PlayerInfo pi2 = master.Game.AddPlayer(false);
            string message = @"{
                ""type"": ""REFRESH_STATE_REQUEST"",
                ""senderId"":""" + pi.Id + @"""
                }";
            var response = master.ReceiveMessage(message);
            JObject responseObject = JObject.Parse(response);
            Assert.AreEqual("REFRESH_STATE_RESPONSE",responseObject["type"]);
            PlayerInfoShortened[] infoShortened=new PlayerInfoShortened[]{
                new PlayerInfoShortened(){ Id=pi.Id, X=pi.X, Y=pi.Y},
                new PlayerInfoShortened(){Id=pi2.Id, X=pi2.X, Y=pi2.Y}};
            response = master.ReceiveMessage(message);
            responseObject = JObject.Parse(response);
            RefreshResponse msg = responseObject.ToObject<RefreshResponse>(serializer);
            Assert.AreEqual(infoShortened[0].X, msg.Payload.PlayerInfoShortened[0].X);
        }
        [TestMethod]
        public void PickUpResponseTest()
        {
            GameMaster.Master master=new GameMaster.Master();
            PlayerInfo pi= master.Game.AddPlayer(false);
            string message = @"{
                ""type"": ""PICK_UP_PIECE_REQUEST"",
                ""senderId"":""" + pi.Id + @"""
                }";
            var response = master.ReceiveMessage(message);
            JObject responseObject = JObject.Parse(response);
            Assert.AreEqual("ACTION_INVALID",responseObject["type"]);
            PieceInfo pinfo = master.Game.AddPiece();
            master.Game.Pieces[0].X=pi.X;
            master.Game.Pieces[0].Y=pi.Y;
            response = master.ReceiveMessage(message);
            responseObject = JObject.Parse(response);
            Assert.AreEqual("PICK_UP_PIECE_RESPONSE",responseObject["type"]);
        }
    }
}