﻿using System;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Tests
{
    [TestClass]
    public class PlayerTest
    {
        [TestMethod]
        public void DestroyPiece()
        {
            Player.Player p = new Player.Player();
            p.Id = "1";
            p.carryPiece = true;
            p.pieceId = "1";

            p.DestroyPiece();

            Assert.AreEqual(false, p.carryPiece);
            Assert.AreEqual("", p.pieceId);
        }

        [TestMethod]
        public void TestPiece()
        {
            Player.Player p = new Player.Player();
            p.Id = "1";
            p.carryPiece = true;
            p.pieceId = "1";

            bool isGood = p.TestPiece();

            Assert.AreEqual(false, isGood);
        }

        [TestMethod]
        public void Move()
        {
            Player.Player p = new Player.Player();
            p.Id = "1";

            int distance = p.Move(Common.Direction.right);

            Assert.AreEqual(2, distance);
        }

        [TestMethod]
        public void LeavePiece()
        {
            Player.Player p = new Player.Player();
            p.Id = "1";
            p.carryPiece = true;
            p.pieceId = "1";

            bool goalDiscovered = p.LeavePiece();

            Assert.AreEqual(false, goalDiscovered);

        }

        [TestMethod]
        public void TakePiece()
        {
            Player.Player p = new Player.Player();
            p.Id = "1";

            p.TakePiece();

            Assert.AreEqual(true, p.carryPiece);
            Assert.AreEqual("", p.pieceId);
        }

        [TestMethod]
        public void DiscoverSurrounding()
        {
            Player.Player p = new Player.Player();
            p.Id = "1";

            p.DiscoverSurrounding();


        }
    }
}
